package src;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.security.SecureRandom;
import java.util.Random;


public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }

    private BorderPane borderPane = new BorderPane();
    private VBox vBox = new VBox();
    private HBox hBox = new HBox();

    private Label descriptionLabel = new Label("You win if the same color is shown in all boxes.");
    private Rectangle[] rectangles = new Rectangle[] {new Rectangle(100, 200), new Rectangle(100, 200), new Rectangle(100, 200)};

    @Override
    public void start(Stage primaryStage) throws Exception {
        descriptionLabel.setFont(Font.font(16)); //Set font size to 16

        BorderPane.setAlignment(hBox, Pos.CENTER);
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(10); //Space between boxes 10px

        vBox.setSpacing(20); //Space between Label, HBox (Containing the boxes) and the button
        vBox.setAlignment(Pos.CENTER);
        borderPane.setCenter(vBox);

        //Adds all rectangles to the HBox
        for (Rectangle rectangle : rectangles) {
            hBox.getChildren().add(rectangle);
        }
        changeColors(); //Randomizes the colors

        Button rollButton = new Button("Roll again");
        vBox.getChildren().add(descriptionLabel);
        vBox.getChildren().add(hBox);
        vBox.getChildren().add(rollButton);

        rollButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            changeColors(); //Changes the colors if the button is pressed
        });


        Scene scene = new Scene(borderPane, 400, 300);
        primaryStage.setTitle("Color Roulette");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void changeColors() {
        Random rand = new SecureRandom(); //Use secure random to archive true randomness, ThreadLocalRandom is insufficient
        Color[] colors = new Color[] {Color.GREEN, Color.BLUE, Color.RED};

        for (Rectangle rectangle : rectangles) {
            int randomNumber = rand.nextInt(colors.length); //Generates a random value between 0 and colors.length - 1
            rectangle.setFill(colors[randomNumber]);
        }

        if(isSameColor()) {
            showWinScreen();
        }
    }

    private boolean isSameColor() {
        Paint firstColor = null;

        for (Rectangle rectangle : rectangles) {
            if(firstColor == null) {
                firstColor = rectangle.getFill();
            }

            if(!firstColor.equals(rectangle.getFill())) {
                return false;
            }
        }
        return true;
    }

    private void showWinScreen() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "You have won!");
        alert.setHeaderText("");
        alert.showAndWait();
    }
}
