# Color Roulette

This applications displays (3) boxes which can be have colors defined in an Array (currently RED, BLUE, GREEN).<br>
When pressing the "Roll again" button, the colors get reshuffled.<br> 
If the colors of <b>all (3) boxes</b> match the user wins.


Dieses Programm zeigt (3) Boxen welche verschiedene Farben besitzen können. <br>
Die Farben aus denen gewählt wird sind in einem Array hinterlegt (gerade Rot, Blau, Grün).<br>
Wenn der "Roll again" Button gedrückt wird werden die Farben neu gemischt.<br>
Wenn die Farben aller (3) Boxen gleich sind gewinnt der User.
