# Leaderboard

This application prompts the user to enter his name. <br>
After that the application greets the user with his/her name and tells him to enter
a leaderboard entry consisting of a name (Constantin) and a score (13.37)

<br>
If the entry list consists of <b>one or more entries</b> the user can chose to add another one or print the leaderboard.

<br><br>
Leaderboard example:
1. Jona (1337.0)
2. Fabian (12.3)
3. Larissa (12.0)
4. Lukas (10.8)