package me.constantinschreiber.Model;

/**
 * @author Constantin Schreiber
 * @since 12/05/2019
 */
public class LeaderboardEntry {
    private String name;
    private double score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
