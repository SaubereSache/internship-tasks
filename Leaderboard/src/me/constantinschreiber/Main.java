package me.constantinschreiber;

/**
 * @author Constantin Schreiber
 * @since 12/05/2019
 */
public class Main {

    /**
     * This program lets the user input names with scores and prints them in a list.
     * It uses more "advanced" techniques and contains basic object oriented programming (OOP)
     * @param args The program arguments supplied by the command line interface (CLI)
     */
    public static void main(String[] args) {
        System.out.println("Welcome to the leaderboard app!");

        /*
        Create a new instance of the leaderboard class and initializes it,
         which means the user will be prompted for input since this part of the code is located in the constructor.
        */
        Leaderboard leaderboard = new Leaderboard();
        leaderboard.printLeaderboard(); //Prints the leaderboard

    }
}
