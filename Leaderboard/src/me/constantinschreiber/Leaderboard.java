package me.constantinschreiber;

import me.constantinschreiber.Model.LeaderboardEntry;

import java.util.*;

/**
 * @author Constantin Schreiber
 * @since 12/05/2019
 */
public class Leaderboard {
    private final Scanner scanner = new Scanner(System.in);
    private List<LeaderboardEntry> leaderboardEntries = new ArrayList<>(); //A list containing Leaderboard objects

    //Constructor
    public Leaderboard() {
        System.out.print("Please input your name: ");
        String username = scanner.nextLine(); //We are not using scanner.next() because it ignores new lines (returns), which will turn into a problem if you are listening for such

        clearScreen();
        System.out.println("Hello " + username + ". Let's start with adding some leaderboard entries.");

        System.out.println("Press RETURN to continue...");
        scanner.nextLine(); //Waits until user presses return
        clearScreen();

        retrieveScoreEntries();

    }

    /**
     * This method retrieves the user input
     */
    private void retrieveScoreEntries() {
        while (true) {
            System.out.println("You already added " + leaderboardEntries.size() + " entries.");

            if(leaderboardEntries.size() > 0) {
                System.out.println("Do you want to add another entry? (y/n)");
                if(!scanner.nextLine().toLowerCase().contains("y")) {
                    break; //break exits the while(true) loop
                }
            }
            LeaderboardEntry leaderboardEntry = new LeaderboardEntry();
            System.out.print("Please enter a name: ");
            leaderboardEntry.setName(scanner.nextLine());

            System.out.print("Please enter a score: ");
            leaderboardEntry.setScore(scanner.nextDouble());
            scanner.nextLine(); //Eat newline from score entry

            leaderboardEntries.add(leaderboardEntry);

            clearScreen();
        }
    }

    /**
     * This method prints the leaderboard sorted by score (descending)
     */
    public void printLeaderboard() {
        //Sorts the list by score in descending order. See https://stackoverflow.com/a/18312192
        leaderboardEntries.sort(Comparator.comparing(LeaderboardEntry::getScore).reversed());

        int place = 1;
        for (LeaderboardEntry leaderboardEntry : leaderboardEntries) {
            System.out.println(place + ". " + leaderboardEntry.getName() + " (" + leaderboardEntry.getScore() + ")");
            place++;
        }
    }

    /**
     * Pseudo screen clear
     * This function prints a new line 40 times
     */
    private static void clearScreen() {
        for(int i = 0; i < 40; i++) {
            System.out.println();
        }
    }
}
