# Internship Tasks

This repository contains code/projects which could be used to distract an intern.<br>
Code quality may suffer as projects were made with simplicity in mind to help facilitate the learning process.<br>

All projects were created using <b>IntelliJ IDEA</b>. Workspace configurations are provided.<br>
IntelliJ IDEA Download: https://www.jetbrains.com/idea/download/ <br>
JDK8 is required.

<br><br>
If you want to add own projects which could assist in learning Java, feel free to contact me at mail@constantinschreiber.me<br>
If you have questions regarding the projects code, feel free to contact me at mail@constantinschreiber.me
