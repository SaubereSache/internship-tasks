import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;

public class Main extends Application {

    public static void main(String[] args) {
        launch();
    }

    private double xOffset = 0;
    private double yOffset = 0;

    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane mainGridPane = new GridPane();
        GridPane applicationPane = new GridPane();

        // Top Bar Pane
        Pane bar = new Pane();

        applicationPane.setPadding(new Insets(25, 25, 25, 45));
        applicationPane.setHgap(15);
        applicationPane.setVgap(8);

        // Radio Group
        ToggleGroup radioButtonToggleGroup = new ToggleGroup();

        // Close Icon
        Image imageClose = new Image(getClass().getResource("/images/close.png").toString());
        ImageView closeImage = new ImageView();
        closeImage.setImage(imageClose);
        closeImage.setLayoutX(512);
        closeImage.setLayoutY(2);
        closeImage.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> System.exit(0));
        closeImage.setId("cursor");

        // Top Bar
        Label labelBarTitle = new Label("Physics Calculator");
        labelBarTitle.setPadding(new Insets(5, 0, 0, 10));
        bar.setId("bar");
        bar.getChildren().addAll(labelBarTitle, closeImage);
        bar.setPrefWidth(540);
        bar.setMaxHeight(28);
        GridPane.setValignment(bar, VPos.TOP);

        // Title Label
        Label titleLabel = new Label("Which unit do you wan't to calculate?");
        GridPane.setMargin(titleLabel, new Insets(40, 0, 0, 0));
        titleLabel.setPadding(new Insets(20, 0, 0, 45));

        // Spannung/Volt
        RadioButton voltageRadio = new RadioButton("Voltage U");
        voltageRadio.setToggleGroup(radioButtonToggleGroup);
        TextField voltageTextField = new TextField();
        Label voltLabel = new Label("Volt");

        // Stromstärke/Ampere
        RadioButton currentRadio = new RadioButton("Current I");
        currentRadio.setToggleGroup(radioButtonToggleGroup);
        TextField currentTextField = new TextField();
        Label ampereLabel = new Label("Ampere");

        // Widerstand/Ohm
        RadioButton resistanceRadio = new RadioButton("Resistance R");
        resistanceRadio.setToggleGroup(radioButtonToggleGroup);
        TextField resistanceTextField = new TextField();
        Label labelOhm = new Label("Ohm");

        // Berechnen Button
        Button buttonBerechnen = new Button("Calculate");
        buttonBerechnen.setPrefWidth(100);
        GridPane.setMargin(buttonBerechnen, new Insets(10, 0, 0, 0));

        radioButtonToggleGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
            changeTextFieldState(voltageRadio, voltageTextField);
            changeTextFieldState(currentRadio, currentTextField);
            changeTextFieldState(resistanceRadio, resistanceTextField);
        });

        // Button Event
        buttonBerechnen.setOnAction(event -> {
            if (radioButtonToggleGroup.getSelectedToggle() == null) return;

            if(voltageRadio.isSelected()) {
                double voltage = Double.valueOf(currentTextField.getText()) * Double.valueOf(resistanceTextField.getText());
                voltageTextField.setText(String.valueOf(voltage));

            } else if(currentRadio.isSelected()) {
                double current = Double.valueOf(voltageTextField.getText()) / Double.valueOf(resistanceTextField.getText());
                currentTextField.setText(String.valueOf(current));
            } else if(resistanceRadio.isSelected()) {
                double resistance = Double.valueOf(voltageTextField.getText()) / Double.valueOf(currentTextField.getText());
                resistanceTextField.setText(String.valueOf(resistance));
            }
        });

        mainGridPane.add(titleLabel, 0, 0);
        mainGridPane.add(bar, 0, 0);

        applicationPane.add(voltageRadio, 0, 0);
        applicationPane.add(currentRadio, 0, 1);
        applicationPane.add(resistanceRadio, 0, 2);
        applicationPane.add(voltageTextField, 1, 0);
        applicationPane.add(currentTextField, 1, 1);
        applicationPane.add(resistanceTextField, 1, 2);
        applicationPane.add(buttonBerechnen, 0, 3);
        applicationPane.add(voltLabel, 2, 0);
        applicationPane.add(ampereLabel, 2, 1);
        applicationPane.add(labelOhm, 2, 2);

        mainGridPane.add(applicationPane, 0, 1);


        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPrefWidth(160);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPrefWidth(200);

        applicationPane.getColumnConstraints().addAll(col1, col2);


        Scene scene = new Scene(mainGridPane, 540, 285);

        // Borderless window
        primaryStage.initStyle(StageStyle.TRANSPARENT);

        // Make draggable
        bar.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });
        bar.setOnMouseDragged(event -> {
            primaryStage.setX(event.getScreenX() - xOffset);
            primaryStage.setY(event.getScreenY() - yOffset);
        });

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void changeTextFieldState(RadioButton radio, TextField textField) {
        if(radio.isSelected()) {
            textField.setEditable(false);
        } else {
            textField.setEditable(true);
        }
        textField.clear();
    }
}
