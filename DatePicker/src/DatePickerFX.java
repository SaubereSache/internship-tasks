import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.MonthDay;

public class DatePickerFX extends Application {

    /*
     * Containers
     */
    private BorderPane borderPane = new BorderPane();
    private HBox hBox = new HBox();
    private VBox vBoxVon = new VBox();
    private VBox vBoxBis = new VBox();

    /*
     * Nodes
     */
    private Label labelUeberschrift = new Label("Wie viel Tage noch?");
    private Label labelVon = new Label("von: ");
    private Label labelBis = new Label("bis: ");
    private DatePicker datePickerVon = new DatePicker();
    private DatePicker datePickerBis = new DatePicker();
    private Button buttonTageBerechnen = new Button("Tage berechnen");

    @Override
    public void start(Stage primaryStage) {

        // Abstand zwischen den DatePicker
        hBox.setSpacing(10);

        // Setzt die größe von den VBoxen
        vBoxBis.setMaxHeight(150);
        vBoxVon.setMaxHeight(150);

        // Fügt die Nodes & Container zu den jeweiligen Containern
        vBoxVon.getChildren().addAll(labelVon, datePickerVon);
        vBoxBis.getChildren().addAll(labelBis, datePickerBis);
        hBox.getChildren().addAll(vBoxVon, vBoxBis);

        // Setzt die Nodes & Container in die Mitte
        BorderPane.setAlignment(labelUeberschrift, Pos.CENTER);
        BorderPane.setAlignment(hBox, Pos.CENTER);
        BorderPane.setAlignment(buttonTageBerechnen, Pos.CENTER);
        hBox.setAlignment(Pos.CENTER);

        // Setzt das DatePicker "von" Feld auf den heutigen Tag
        datePickerVon.setValue(LocalDate.now());

        //Erkennt ob das Bis Datum in der Vergangenheit ist und setzt es in die Gegenwart
        datePickerVon.setOnAction(event -> {
            if(datePickerBis.getValue().isBefore(datePickerVon.getValue())) {
                datePickerBis.setValue(datePickerVon.getValue().plusDays(1));
            }
        });

        // Setzt das DatePicker "bis" Feld einen Tag später
        datePickerBis.setValue(datePickerVon.getValue().plusDays(1));

        // Setzt die day cell factory
        datePickerVonDayCell();
        datePickerBisDayCell();

        // Button Event (Tage Berechnung)
        buttonTageBerechnen.setOnAction(event -> {
            double datumVon = datePickerVon.getValue().toEpochDay();
            double datumBis = datePickerBis.getValue().toEpochDay();
            int tage = (int) Math.abs(datumVon - datumBis);
            labelUeberschrift.setText(tage + " Tag(e)");
        });

        // Setzt einen Abstand am Rand
        borderPane.setPadding(new Insets(10));

        // Setzt die Nodes & Container zum BorderPane
        borderPane.setTop(labelUeberschrift);
        borderPane.setCenter(hBox);
        borderPane.setBottom(buttonTageBerechnen);

        Scene scene = new Scene(borderPane, 495, 320);
        primaryStage.setTitle("DatePickerFX");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void datePickerVonDayCell() {
        // Erstellt eine benutzerdefinierte cell factory
        Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        highlightWeekend(item, this);
                        highlightBirthday(item, this);
                    }
                };
            }
        };

        // Fügt die day cell zum DatePicker hinzu
        datePickerVon.setDayCellFactory(dayCellFactory);
    }

    private void datePickerBisDayCell() {
        // Erstellt eine benutzerdefinierte cell factory
        Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        // Überprüft beim "von" DatePicker das gesetzte Datum und deaktiviert die vorherigen Tage
                        if (item.isBefore(datePickerVon.getValue().plusDays(1))) {
                            setDisable(true);
                            // Färbt den Hintergrund vom Datum
                            setStyle("-fx-background-color: #EEEEEE;");
                        }

                        highlightWeekend(item, this);
                        highlightBirthday(item, this);
                    }
                };
            }
        };

        // Fügt die day cell zum DatePicker hinzu
        datePickerBis.setDayCellFactory(dayCellFactory);
    }

    private void highlightWeekend(LocalDate localDate, DateCell dateCell) {
        DayOfWeek day = DayOfWeek.from(localDate);
        if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY) {
            // Stellt die Tage am Wochenende blau dar
            dateCell.setTextFill(Color.BLUE);
        }
    }

    private void highlightBirthday(LocalDate localDate, DateCell dateCell) {
        // Stellt einen bestimmten Tag im Monat/Tag anders da
        if (MonthDay.from(localDate).equals(MonthDay.of(6, 25))) {
            // Beim Maus hovern wird ein Tooltip/Text angezeigt
            dateCell.setTooltip(new Tooltip("Happy Birthday!"));
            // Färbt den Hintergrund vom Datum
            dateCell.setStyle("-fx-background-color: #ff4444;");
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}