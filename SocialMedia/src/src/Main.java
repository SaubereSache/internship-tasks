package src;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.*;
import java.net.URI;

public class Main extends Application {
    public static void main(String[] args) {
        launch();
    }

    private BorderPane borderPane = new BorderPane();
    private HBox hBox = new HBox();
    private VBox snapchatBox = new VBox();
    private VBox instagramBox = new VBox();

    @Override
    public void start(Stage primaryStage) throws Exception {
        hBox.setSpacing(20);
        instagramBox.setMaxHeight(150);
        snapchatBox.setMaxHeight(150);

        Image instagramImage = new Image(getClass().getResource("/images/instagram.png").toString(), 150, 150, false, false);
        ImageView instagramImageView = new ImageView();
        instagramImageView.setImage(instagramImage);
        instagramImageView.setPreserveRatio(true);

        instagramImageView.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            try {
                Desktop.getDesktop().browse(new URI("https://instagram.com/YourUsernameHere"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        Image snapchatImage = new Image(getClass().getResource("/images/snapchat.png").toString(), 150, 150, false, false);
        ImageView snapchatImageView = new ImageView();
        snapchatImageView.setImage(snapchatImage);
        snapchatImageView.setPreserveRatio(true);

        snapchatImageView.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            try {
                Desktop.getDesktop().browse(new URI("https://snapchat.com/add/YourUsernameHere"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        snapchatBox.getChildren().addAll(instagramImageView);
        instagramBox.getChildren().addAll(snapchatImageView);
        hBox.getChildren().addAll(snapchatBox, instagramBox);


        BorderPane.setAlignment(hBox, Pos.CENTER);
        hBox.setAlignment(Pos.CENTER);

        borderPane.setPadding(new Insets(10));
        borderPane.setCenter(hBox);
        borderPane.setStyle("-fx-background-color: #FFFFFF;"); //Optional

        Scene scene = new Scene(borderPane, 495, 210);
        primaryStage.setTitle("Social Media App");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
