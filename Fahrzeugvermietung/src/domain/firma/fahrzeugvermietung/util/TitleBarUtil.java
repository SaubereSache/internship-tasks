package domain.firma.fahrzeugvermietung.util;

import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Custom TitleBar Util
 *
 * @author Jannik Buscha
 */
public class TitleBarUtil {

    private static double xOffset = 0, yOffset = 0, barHeight = 32, iconWidth = 46;

    // Bar
    private static Pane bar;

    private static HBox hBox;

    // Button Icons
    private static Button btnClose, btnMaximize, btnMinimize;

    /**
     * Handle when the window can be minimized
     *
     * @param stage to minimize
     */
    private static void handleMinimize(Stage stage) {
        // Set graphic
        btnMinimize.setGraphic(new ImageView(new Image("https://i.imgur.com/lyKknxN.png")));

        // Set click event
        btnMinimize.setOnMouseClicked(event -> stage.setIconified(true));
    }

    /**
     * Handle when the window can be maximized
     *
     * @param stage to maximized
     */
    private static void handleMaximize(Stage stage) {
        // Set graphic
        btnMaximize.setGraphic(new ImageView(new Image("https://i.imgur.com/TEwO72t.png")));

        // Set click event
        btnMaximize.setOnMouseClicked(event -> {
            // Check if stage maximized
            if (stage.isMaximized()) {
                // Disable maximize
                stage.setMaximized(false);

                // Replace graphic
                btnMaximize.setGraphic(new ImageView(new Image("https://i.imgur.com/TEwO72t.png")));
            } else {
                // Set maximize
                stage.setMaximized(true);

                // Replace graphic
                btnMaximize.setGraphic(new ImageView(new Image("https://i.imgur.com/1YmTDKN.png")));
            }
        });
    }

    /**
     * Handle when the window can be closed
     */
    private static void handleClose() {
        // Set graphic
        btnClose.setGraphic(new ImageView(new Image("https://i.imgur.com/mv5QSzg.png")));

        // Set click event
        btnClose.setOnMouseClicked(event -> System.exit(0));
    }

    /**
     * Handle when the window can be double click to maximize or minimize
     *
     * @param stage to maximize or minimize
     */
    private static void doubleClickWindow(Stage stage) {
        // Set click event
        bar.setOnMouseClicked(mouseEvent -> {
            // Check if primary button
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                // Check if click count equals 2
                if (mouseEvent.getClickCount() == 2) {
                    // Set opposite maximize state
                    stage.setMaximized(!stage.isMaximized());
                }
            }
        });
    }

    /**
     * Make window draggable
     *
     * @param stage of the window
     */
    private static void draggable(Stage stage) {
        // Add event to make window draggable
        bar.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();
        });

        // Add event to make window draggable
        bar.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - xOffset);
            stage.setY(event.getScreenY() - yOffset);
        });
    }

    /**
     * Adding style to the TitleBarUtil
     */
    private static void style() {
        // Add style to hbox
        hBox.getStylesheets().add(TitleBarUtil.class.getResource("/css/style.css").toExternalForm());

        // Set pref width to 5000
        bar.setPrefWidth(5000);

        // Add style classes
        hBox.getStyleClass().add("hBox");
        btnClose.getStyleClass().add("buttonIcon");
        btnMaximize.getStyleClass().add("buttonIcon");
        btnMinimize.getStyleClass().add("buttonIcon");

        // Set height
        bar.setMinHeight(barHeight);
        bar.setPrefHeight(barHeight);
        bar.setMaxHeight(barHeight);

        // Change button width for nodes
        changeButtonWidth(btnClose, iconWidth);
        changeButtonWidth(btnMaximize, iconWidth);
        changeButtonWidth(btnMinimize, iconWidth);

        // Set alignment
        hBox.setAlignment(Pos.TOP_RIGHT);

        // Set position
        GridPane.setValignment(hBox, VPos.TOP);
    }

    /**
     * Change the width of button
     *
     * @param btn   to change
     * @param width of the button to change
     */
    private static void changeButtonWidth(Button btn, double width) {
        btn.setMinWidth(width);
        btn.setMaxWidth(width);
        btn.setPrefWidth(width);
    }

    /**
     * Show the TitleBarUtil
     *
     * @param grd       of the TitleBarUtil
     * @param stage     of the TitleBarUtil
     * @param minimize  the TitleBarUtil?
     * @param maximize  the TitleBarUtil?
     * @param close     the TitleBarUtil?
     * @param draggable the TitleBarUtil?
     */
    public static void show(GridPane grd, Stage stage, boolean minimize, boolean maximize, boolean close, boolean draggable) {
        // Create new objects
        bar = new Pane();
        hBox = new HBox();
        btnClose = new Button();
        btnMaximize = new Button();
        btnMinimize = new Button();

        // Style objects
        style();

        // Check if close button exists
        if (close) {
            handleClose();
        } else {
            // Change button width to 0
            changeButtonWidth(btnClose, 0);
        }

        // Check if window can be maximized
        if (maximize) {
            // Add maximized buttons to stage
            handleMaximize(stage);

            // Add double click to stage
            doubleClickWindow(stage);

            // Make resizeable
            ResizeUtil.addResizeListener(stage, 1280, 720, Double.MAX_VALUE, Double.MAX_VALUE);
        } else {
            // Change button width to 0
            changeButtonWidth(btnMaximize, 0);
        }

        // Check if window can be minimize
        if (minimize) {
            // Add minimize button to stage
            handleMinimize(stage);
        } else {
            // Change button width to 0
            changeButtonWidth(btnMinimize, 0);
        }

        // Check if window can be draggable
        if (draggable) {
            draggable(stage);
        }

        // Add children to HBox
        hBox.getChildren().addAll(bar, btnMinimize, btnMaximize, btnClose);

        // Add HBox to grid pane
        grd.getChildren().addAll(hBox);
    }

}