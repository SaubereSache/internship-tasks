package domain.firma.fahrzeugvermietung.controller;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import domain.firma.fahrzeugvermietung.util.TimelineUtil;
import domain.firma.fahrzeugvermietung.util.TitleBarUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class FahrzeugvermietungController implements Initializable {

    @FXML
    GridPane grdMain;

    @FXML
    TableView tvPKW, tvLKW, tvAnhaenger;

    @FXML
    TableColumn tcCarPKW, tcMitfahrerPKW, tcPricePKW, tcCarLKW, tcGroesseLKW, tcPriceLKW, tcCarAnhaenger, tcLaengeAnhaenger, tcPriceAnhaenger;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        pkwTable();
        lkwTable();
        anhaengerTable();

        new TimelineUtil(1L, e -> TitleBarUtil.show(grdMain, (Stage) grdMain.getScene().getWindow(), true, true, true, true)).start();
    }

    private void pkwTable() {
        // Disable table sorting
        tcCarPKW.setSortable(false);
        tcMitfahrerPKW.setSortable(false);
        tcPricePKW.setSortable(false);

        // Add listener to disable sorting
        tvPKW.widthProperty().addListener((source, oldWidth, newWidth) -> {
            TableHeaderRow header = (TableHeaderRow) tvPKW.lookup("TableHeaderRow");
            header.reorderingProperty().addListener((observable, oldValue, newValue) -> header.setReordering(false));
        });
    }

    private void lkwTable() {
        // Disable table sorting
        tcCarLKW.setSortable(false);
        tcGroesseLKW.setSortable(false);
        tcPriceLKW.setSortable(false);

        // Add listener to disable sorting
        tvLKW.widthProperty().addListener((source, oldWidth, newWidth) -> {
            TableHeaderRow header = (TableHeaderRow) tvLKW.lookup("TableHeaderRow");
            header.reorderingProperty().addListener((observable, oldValue, newValue) -> header.setReordering(false));
        });
    }

    private void anhaengerTable() {
        // Disable table sorting
        tcCarAnhaenger.setSortable(false);
        tcLaengeAnhaenger.setSortable(false);
        tcPriceAnhaenger.setSortable(false);

        // Add listener to disable sorting
        tvAnhaenger.widthProperty().addListener((source, oldWidth, newWidth) -> {
            TableHeaderRow header = (TableHeaderRow) tvAnhaenger.lookup("TableHeaderRow");
            header.reorderingProperty().addListener((observable, oldValue, newValue) -> header.setReordering(false));
        });
    }

}
