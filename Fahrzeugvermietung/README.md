**Aufgaben**

Aufgabe 1)
-	Erstelle jeweils zu den Bereichen PKW, LWK und Anhänger drei Beispiel Tabellen Einträge. Nutze für die Spalte mit den leeren Namen einheitliche Bilder (z.B. von https://www.volkswagen.at/modelle/karosserie/suv) und für die anderen Felder realitätsnahe Daten.
-	Sobald man über den jeweiligen Tabellen Eintrag mit der Mouse hovered soll ein Text erscheinen um welches Fahrzeug es sich handelt (Tooltip).

Aufgabe 2)
-	Mach den Tabellen Eintrag anklickbar und lass ein extra Fenster öffnen mit Zusatz Informationen (Alert Dialog).
-	Gestalte diesen Dialog entsprechend dem Design mit CSS.

Aufgabe 3)
-	Erstelle ein Label im integrierten Scene Builder mit dem Namen „Noch heute zum fairen Preis buchen“ über dem TabPane (Beachte es soll Responsive sein und zum Design passen).

Credits to @kuqs